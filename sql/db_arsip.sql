-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table db_arsip.arsip
CREATE TABLE IF NOT EXISTS `arsip` (
  `id_arsip` int(11) NOT NULL AUTO_INCREMENT,
  `nama_arsip` varchar(100) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `tgl_arsip` date NOT NULL,
  `foto` varchar(100) NOT NULL,
  PRIMARY KEY (`id_arsip`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- Dumping data for table db_arsip.arsip: ~4 rows (approximately)
/*!40000 ALTER TABLE `arsip` DISABLE KEYS */;
INSERT INTO `arsip` (`id_arsip`, `nama_arsip`, `id_bidang`, `id_kategori`, `tgl_arsip`, `foto`) VALUES
	(30, 'Data Ormas tahun 2013', 3, 11, '2020-03-19', '78280657913-20200319-064356.xlsx'),
	(31, 'Permohonan audensi', 1, 9, '2020-03-19', '83810612927-20200319-064455.jpg'),
	(32, 'Syarat pendaftaran skt ormas', 3, 7, '2020-02-02', '22404063590-20200319-064634.docx'),
	(33, '666', 4, 8, '2020-03-19', '80655832485-20200319-113733.jpg'),
	(34, 'buku', 1, 8, '2020-11-11', ''),
	(35, 'buku2', 2, 9, '2020-11-19', '');
/*!40000 ALTER TABLE `arsip` ENABLE KEYS */;

-- Dumping structure for table db_arsip.bidang
CREATE TABLE IF NOT EXISTS `bidang` (
  `id_bidang` int(11) NOT NULL AUTO_INCREMENT,
  `bidang` varchar(20) NOT NULL,
  PRIMARY KEY (`id_bidang`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table db_arsip.bidang: ~4 rows (approximately)
/*!40000 ALTER TABLE `bidang` DISABLE KEYS */;
INSERT INTO `bidang` (`id_bidang`, `bidang`) VALUES
	(1, 'umum'),
	(2, 'politik'),
	(3, 'ideologi'),
	(4, 'kewaspadaan');
/*!40000 ALTER TABLE `bidang` ENABLE KEYS */;

-- Dumping structure for table db_arsip.kategori
CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(25) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table db_arsip.kategori: ~5 rows (approximately)
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` (`id_kategori`, `kategori`) VALUES
	(7, 'formulir'),
	(8, 'surat keluar'),
	(9, 'surat masuk'),
	(10, 'surat teguran'),
	(11, 'Berkas data');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;

-- Dumping structure for table db_arsip.user
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama_lengkap` varchar(25) NOT NULL,
  `level` varchar(10) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table db_arsip.user: ~3 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id_user`, `username`, `password`, `nama_lengkap`, `level`) VALUES
	(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Ade Saputra', 'Admin'),
	(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'Pimpinan A', 'User'),
	(6, 'pegawai', '047aeeb234644b9e2d4138ed3bc7976a', 'pegawai A', 'User');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
