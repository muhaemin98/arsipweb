<?php

class Pengguna_model extends CI_Model
{

    public $table = 'user';
    public $id = 'id_user';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function tampil_data()
    {
        return $this->db->get($this->table);
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function delete($id)
    { 
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}
