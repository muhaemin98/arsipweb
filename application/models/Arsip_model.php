<?php 

class Arsip_model extends CI_Model{

    public $table = 'arsip';
    public $id = 'id_arsip';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

     // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function tampil_data(){
        return $this->db->get($this->table);
    }

    function delete($id)
    {
        $row=$this->db->where($this->id, $id)->get($this->table)->row();

        if ($row->foto != ""){
        unlink("./assets/file_upload/$row->foto");

        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        }else{

        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        }
    }

    function laporan_admin()
    {
        return $this->db->query('SELECT *,a.foto FROM arsip a LEFT JOIN kategori b ON a.id_kategori=b.id_kategori LEFT JOIN bidang c ON a.id_bidang=c.id_bidang order by a.id_arsip DESC');
    }

    function diagram()
    {
        return $this->db->query('SELECT a.id_bidang, b.bidang, count(a.id_bidang) as count from arsip a, bidang b WHERE a.id_bidang=b.id_bidang GROUP BY a.id_bidang');
    }

    function perkategori()
    {
        $set_lap2 = $this->session->userdata('kategori');

        return $this->db->query("SELECT * FROM arsip a LEFT JOIN kategori b ON a.id_kategori=b.id_kategori LEFT JOIN bidang c ON a.id_bidang=c.id_bidang WHERE a.id_kategori = '$set_lap2'");
    }

    function pertgl()
    {
        $set_lap2['tanggal_awal'] = $this->session->userdata('tanggal_awal');
        $set_lap2['tanggal_akhir'] = $this->session->userdata('tanggal_akhir');

        return $this->db->query("SELECT * FROM arsip a LEFT JOIN kategori b ON a.id_kategori=b.id_kategori LEFT JOIN bidang c ON a.id_bidang=c.id_bidang WHERE a.tgl_arsip BETWEEN '".$set_lap2['tanggal_awal']."' AND '".$set_lap2['tanggal_akhir']."' ORDER BY a.id_arsip DESC");
    }
    
}