<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lap_tgl extends CI_Controller {


	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('arsip_helper');
		$this->load->model('Arsip_model');
	}
	

	public function index()
	{
		if($this->session->userdata('logged_in')!="")
		{
				$d['arsip'] = $this->Arsip_model->laporan_admin();

				$d['bidang'] = $this->db->get('bidang');
				$d['kategori'] = $this->db->get('kategori');
					
				 $this->load->view('laporan/lap_tgl',$d);
		}else{
			redirect('app/logout','refresh');
		}	
	}

	public function pertanggal()
	{
		if($this->session->userdata('logged_in')!="")
		{

				$d['arsip'] = $this->Arsip_model->pertgl();

				$d['kategori'] = $this->db->get('kategori');
					
				 $this->load->view('laporan/lap_tgl',$d);
		}
	}

	public function set()
	{
			$sel_lap1['tanggal_awal'] = $this->input->post('tanggal_awal');
			$sel_lap1['tanggal_akhir'] = $this->input->post('tanggal_akhir');
			$this->session->set_userdata($sel_lap1);
			redirect('lap_tgl/pertanggal');
	}
}

