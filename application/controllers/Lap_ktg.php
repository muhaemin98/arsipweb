<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lap_ktg extends CI_Controller {


	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('arsip_helper');
		$this->load->model('Arsip_model');
	}
	

	public function index()
	{
		if($this->session->userdata('logged_in')!="")
		{
				$d['arsip'] = $this->Arsip_model->laporan_admin();

				$d['bidang'] = $this->db->get('bidang');
				$d['kategori'] = $this->db->get('kategori');
					
				 $this->load->view('laporan/lap_ktg',$d);
		}else{
			redirect('app/logout','refresh');
		}	
	}

	public function perkategori()
	{
		if($this->session->userdata('logged_in')!="")
		{
				
				$d['arsip'] = $this->Arsip_model->perkategori();

				$d['kategori'] = $this->db->get('kategori');
					
				 $this->load->view('laporan/lap_ktg',$d);
		}
	}

	public function set()
	{
			$sel_lap1['kategori'] = $this->input->post('kategori');
			$this->session->set_userdata($sel_lap1);
			redirect('lap_ktg/perkategori');
	}
}

