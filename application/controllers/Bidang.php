<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bidang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Bidang_m');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$d['bidang'] = $this->Bidang_m->tampil();
		$this->load->view('bidang/home_v',$d);
		}else{
			redirect('app/logout');
		}
	}

	public function tambah_v()
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$this->load->view('bidang/tambah_v');
		}else{
			redirect('app/logout');
		}
	}

	public function simpan()
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$bidang = $this->input->post('bidang');

		$d=array('bidang' => $bidang);

		$this->Bidang_m->input($d,'bidang');

		$this->session->set_flashdata('message','<div class="alert alert-success" role="alert"> Data Berhasil ditambahkan <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

		redirect('bidang');

		}else{
			redirect('app/logout');
		}

	}

	public function edit($id_bidang)
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$where=array('id_bidang' => $id_bidang);

		$d['bidang'] = $this->Bidang_m->edit_data($where,'bidang');

		$this->load->view('bidang/ubah_v',$d);
		}else{
			redirect('app/logout');
		}
	}

	public function simpan_ubah()
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$id_bidang = $this->input->post('id_bidang');
		$bidang = $this->input->post('bidang');

		$d = array(
			'id_bidang' => $id_bidang,
			'bidang' => $bidang
		);

		$where = array(
			'id_bidang' => $id_bidang
		);

		$this->Bidang_m->edit_simpan($where,$d,'bidang');

		$this->session->set_flashdata('message','<div class="alert alert-warning" role="alert"> Data Berhasil diubah <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

		redirect('bidang');

		}else{
			redirect('app/logout');
		}
	}

	public function hapus($id_bidang)
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$where = array(
			'id_bidang' => $id_bidang
		);

		$this->Bidang_m->hapus($where,'bidang');

		$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert"> Data Berhasil dihapus <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

		redirect('bidang');

		}else{
			redirect('app/logout');
		}

	}
}
