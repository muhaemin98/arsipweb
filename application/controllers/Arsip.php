<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arsip extends CI_Controller {

    public function __construct(){
        parent::__construct();

        $this->load->model('Arsip_model');
        $this->load->helper('arsip_helper');

    }

    public function index() {

      if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="Admin")
      {
        $data['arsip'] = $this->Arsip_model->laporan_admin();

        $this->load->view('arsip/home_v',$data);

      } else if ($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="User") {

        $data['arsip'] = $this->Arsip_model->laporan_admin();

        $this->load->view('arsip_user',$data);
        
      } else{

        redirect('app/logout','refresh');
      }
    }

    public function tambah()    
    {
      if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="Admin")
      {
        $d ['id_param'] ="";
        $d ['nama_arsip'] ="";
        $d ['id_bidang'] ="";
        $d ['id_kategori'] ="";
        $d ['tgl_arsip'] ="";
        $d ['foto'] ="";


        $d ["st"] ="tambah";

        $d['bidang'] = $this->db->get('bidang');
        $d['kategori'] = $this->db->get('kategori');


        $this->load->view('arsip/tambah_v',$d);
      }else{
        redirect('app/logout','refresh');
      }
    }

    public function edit()
    {
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="Admin")
        {
            $id['id_arsip'] = $this->uri->segment(3);
            $q = $this->db->get_where("arsip",$id);
            $d = array();

            foreach($q->result() as $dt)
            {
                $d['id_param'] = $dt->id_arsip;
                $d['nama_arsip'] = $dt->nama_arsip;
                $d['id_bidang'] =$dt->id_bidang;
                $d['id_kategori'] =$dt->id_kategori;
                $d['tgl_arsip'] =$dt->tgl_arsip;
                $d['foto'] =$dt->foto;

            }

                $d['st'] = "edit";

                $d['bidang'] = $this->db->get('bidang');
                $d['kategori'] = $this->db->get('kategori');
                $this->load->view('arsip/ubah_v',$d);

        }else{
            redirect(site_url('arsip'));
        }
    }
    
    public function hapus($id) 
    {
       if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="Admin")
        {

          $row = $this->Arsip_model->get_by_id($id);

          if ($row) {
              $this->Arsip_model->delete($id);
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert"> Data Berhasil dihapus <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
              redirect(site_url('arsip'));
          } else {
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert"> Data tidak ditemukan <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
              redirect(site_url('arsip'));
          }
      } else {
        redirect('app/logout','refresh');
      }
    }

    public function simpan()
    {
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="Admin")
        {
            
            $this->form_validation->set_rules('nama_arsip', 'Nama Arsip ', 'trim|required');

            $id['id_arsip'] = $this->input->post("id_param");
            $st_frame = $this->input->post("frame");
            
            if ($this->form_validation->run() == FALSE)
            {
                $st = $this->input->post('st');
                if($st=="tambah")
                {
                    $d['id_param'] = "";
                    $d['nama_arsip'] ="";
                    $d['id_bidang'] ="";
                    $d['id_kategori'] ="";
                    $d['tgl_arsip'] ="";    
                    $d['foto'] ="";  

                    $d['st'] = $st;

                    $d['bidang'] = $this->db->get('bidang');
                    $d['kategori'] = $this->db->get('kategori');

                    $this->load->view('arsip/tambah_v',$d);
                }
            }
            else
            {
                $st = $this->input->post('st');
                 if($st=="tambah")
                {
                    $in['nama_arsip'] = $this->input->post('nama_arsip');
                    $in['id_bidang'] = $this->input->post('id_bidang');
                    $in['id_kategori'] = $this->input->post('id_kategori');
                    $in['tgl_arsip'] = $this->input->post('tgl_arsip');

                    if(!empty($_FILES['userfile']['name']))
                    {
                        $acak=rand(00000000000,99999999999);
                        $bersih=$_FILES['userfile']['name'];
                        $nm=str_replace(" ","_","$bersih");
                        $pisah=explode(".",$nm);
                        $nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
                        $nama_murni=date('Ymd-His');
                        $ekstensi_kotor = $pisah[1];
                        
                        $file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
                        $file_type_baru = strtolower($file_type);
                        
                        $ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
                        $n_baru = $ubah.'.'.$file_type_baru;
                        
                        $config['upload_path']  = "./assets/file_upload/";
                        $config['allowed_types']= 'gif|jpeg|jpg|png|pdf|mp3|mp4|doc|docx|wma|mp4|xlsx|xls|ppt|pptx';
                        $config['file_name']    = $n_baru;
                        $config['max_size']     = '0';
                        $config['max_width']    = '0';
                        $config['max_height']   = '0';
                 
                        $this->load->library('upload', $config);
                 
                        if ($this->upload->do_upload("userfile")) {
                            $data       = $this->upload->data();
                 
                            /* PATH */
                            $source             = "./assets/file_upload/".$data['file_name'] ;
                 
                            // Permission Configuration
                            chmod($source, 0777) ;
                 
                            /* Resizing Processing */
                            // Configuration Of Image Manipulation :: Static
                            $this->load->library('image_lib') ;
                            $img['image_library'] = 'GD2';
                            $img['create_thumb']  = TRUE;
                            $img['maintain_ratio']= TRUE;
                 
                            /// Limit Width Resize
                            $limit_medium   = 425 ;
                            $limit_thumb    = 220 ;
                 
                            // Size Image Limit was using (LIMIT TOP)
                            $limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;
                 
                            // Percentase Resize
                            if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
                                $percent_medium = $limit_medium/$limit_use ;
                                $percent_thumb  = $limit_thumb/$limit_use ;
                            }
                 
                            //// Making THUMBNAIL ///////
                            $img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
                            $img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;
                 
                            // Configuration Of Image Manipulation :: Dynamic
                            $img['thumb_marker'] = '';
                            $img['quality']      = '100%' ;
                            $img['source_image'] = $source ;
                            $img['new_image']    = $destination_thumb ;
                 
                            // Do Resizing
                            $this->image_lib->initialize($img);
                            $this->image_lib->resize();
                            $this->image_lib->clear() ;
                 
                            ////// Making MEDIUM /////////////
                            $img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
                            $img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;
                 
                            // Configuration Of Image Manipulation :: Dynamic
                            $img['thumb_marker'] = '';
                            $img['quality']      = '100%' ;
                            $img['source_image'] = $source ;
                            $img['new_image']    = $destination_medium ;
                            
                            $in['foto'] = $data['file_name'];
                 
                            // Do Resizing
                            $this->image_lib->initialize($img);
                            $this->image_lib->resize();
                            $this->image_lib->clear() ;
                        }
                    }
                                    
                    $this->db->insert("arsip",$in);

                    $this->session->set_flashdata('message','<div class="alert alert-success" role="alert"> Data Berhasil ditambahkan <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                    redirect('arsip');
                }
            
            }
        } else {
            redirect('app/logout');
        }
    }

    public function simpan_ubah()
    {
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('level')=="Admin")
        {
            
            $this->form_validation->set_rules('nama_arsip', 'Nama Arsip ', 'trim|required');

            $id['id_arsip'] = $this->input->post("id_param");
            $st_frame = $this->input->post("frame");
            $st = $this->input->post('st');
            
            if ($this->form_validation->run() == FALSE)
            {
                if($st=="edit")
                {
                    $id['id_arsip'] = $this->uri->segment(3);
                    $q = $this->db->get_where("arsip",$id);
                    $d = array();

                    foreach($q->result() as $dt)
                    {
                        $d['id_param'] = $dt->id_arsip;
                        $d['nama_arsip'] = $dt->nama_arsip;
                        $d['id_bidang'] =$dt->id_bidang;
                        $d['id_kategori'] =$dt->id_kategori;
                        $d['tgl_arsip'] =$dt->tgl_arsip;
                        $d['foto'] =$dt->foto;

                     }

                        $d['st'] = "edit";

                        $d['bidang'] = $this->db->get('bidang');
                        $d['kategori'] = $this->db->get('kategori');
                        $this->load->view('arsip/ubah_v',$d);
                }
            }
            else
            {
                 if($st=="edit")
                {
                    $upd['nama_arsip'] = $this->input->post('nama_arsip');
                    $upd['id_bidang'] = $this->input->post('id_bidang');
                    $upd['id_kategori'] = $this->input->post('id_kategori');
                    $upd['tgl_arsip'] = $this->input->post('tgl_arsip');

                    if(!empty($_FILES['userfile']['name']))
                    {
                        $acak=rand(00000000000,99999999999);
                        $bersih=$_FILES['userfile']['name'];
                        $nm=str_replace(" ","_","$bersih");
                        $pisah=explode(".",$nm);
                        $nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
                        $nama_murni=date('Ymd-His');
                        $ekstensi_kotor = $pisah[1];
                        
                        $file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
                        $file_type_baru = strtolower($file_type);
                        
                        $ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
                        $n_baru = $ubah.'.'.$file_type_baru;
                        
                        $config['upload_path']  = "./assets/file_upload/";
                        $config['allowed_types']= 'gif|jpeg|jpg|png|pdf|mp3|mp4|doc|docx|wma|mp4|xlsx|xls|ppt|pptx';
                        $config['file_name']    = $n_baru;
                        $config['max_size']     = '0';
                        $config['max_width']    = '0';
                        $config['max_height']   = '0';
                 
                        $this->load->library('upload', $config);
                 
                        if ($this->upload->do_upload("userfile")) {
                            $data       = $this->upload->data();
                 
                            /* PATH */
                            $source             = "./assets/file_upload/".$data['file_name'] ;
                 
                            // Permission Configuration
                            chmod($source, 0777) ;
                 
                            /* Resizing Processing */
                            // Configuration Of Image Manipulation :: Static
                            $this->load->library('image_lib') ;
                            $img['image_library'] = 'GD2';
                            $img['create_thumb']  = TRUE;
                            $img['maintain_ratio']= TRUE;
                 
                            /// Limit Width Resize
                            $limit_medium   = 425 ;
                            $limit_thumb    = 220 ;
                 
                            // Size Image Limit was using (LIMIT TOP)
                            $limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;
                 
                            // Percentase Resize
                            if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
                                $percent_medium = $limit_medium/$limit_use ;
                                $percent_thumb  = $limit_thumb/$limit_use ;
                            }
                 
                            //// Making THUMBNAIL ///////
                            $img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
                            $img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;
                 
                            // Configuration Of Image Manipulation :: Dynamic
                            $img['thumb_marker'] = '';
                            $img['quality']      = '100%' ;
                            $img['source_image'] = $source ;
                            $img['new_image']    = $destination_thumb ;
                 
                            // Do Resizing
                            $this->image_lib->initialize($img);
                            $this->image_lib->resize();
                            $this->image_lib->clear() ;
                 
                            ////// Making MEDIUM /////////////
                            $img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
                            $img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;
                 
                            // Configuration Of Image Manipulation :: Dynamic
                            $img['thumb_marker'] = '';
                            $img['quality']      = '100%' ;
                            $img['source_image'] = $source ;
                            $img['new_image']    = $destination_medium ;
                            
                            $upd['foto'] = $data['file_name'];
                 
                            // Do Resizing
                            $this->image_lib->initialize($img);
                            $this->image_lib->resize();
                            $this->image_lib->clear() ;
                        }
                    }
                                    
                    $this->db->update("arsip", $upd, $id);

                    $this->session->set_flashdata('message','<div class="alert alert-success" role="alert"> Data Berhasil diubah <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                    redirect('arsip');
                }
            
            }
        } else {
            redirect('app/logout');
        }
    }
}