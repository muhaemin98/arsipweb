<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kategori_m');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$d['kategori'] = $this->Kategori_m->tampil();
		$this->load->view('kategori/home_v',$d);
		}else{
			redirect('app/logout');
		}
	}

	public function tambah_v()
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$this->load->view('kategori/tambah_v');
		}else{
			redirect('app/logout');
		}
	}

	public function simpan()
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$kategori = $this->input->post('kategori');

		$d=array('kategori' => $kategori);

		$this->Kategori_m->input($d,'kategori');

		$this->session->set_flashdata('message','<div class="alert alert-success" role="alert"> Data Berhasil ditambahkan <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

		redirect('kategori');

		}else{
			redirect('app/logout');
		}

	}

	public function edit($id_kategori)
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$where=array('id_kategori' => $id_kategori);

		$d['kategori'] = $this->Kategori_m->edit_data($where,'kategori');

		$this->load->view('kategori/ubah_v',$d);
		}else{
			redirect('app/logout');
		}
	}

	public function simpan_ubah()
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$id_kategori = $this->input->post('id_kategori');
		$kategori = $this->input->post('kategori');

		$d = array(
			'id_kategori' => $id_kategori,
			'kategori' => $kategori
		);

		$where = array(
			'id_kategori' => $id_kategori
		);

		$this->Kategori_m->edit_simpan($where,$d,'kategori');

		$this->session->set_flashdata('message','<div class="alert alert-warning" role="alert"> Data Berhasil diubah <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

		redirect('kategori');

		}else{
			redirect('app/logout');
		}
	}

	public function hapus($id_kategori)
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
		$where = array(
			'id_kategori' => $id_kategori
		);

		$this->Kategori_m->hapus($where,'kategori');

		$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert"> Data Berhasil dihapus <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

		redirect('kategori');

		}else{
			redirect('app/logout');
		}

	}
}
