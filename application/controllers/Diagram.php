<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagram extends CI_Controller {

	public function __construct()
	{
		parent::__construct();		
		$this->load->model('Arsip_model');
	}

	public function index()
	{
		if($this->session->userdata('logged_in') != ""){

				    $query= $this->Arsip_model->diagram();

				    	$record = $query->result();
            			$data = [];
            			foreach($record as $row) {
                    	$data['label'][] = $row->bidang;
                    	$data['data'][] = (int) $row->count;
            			}
            			$x['chart_data'] = json_encode($data);

				    	$this->load->view('diagram',$x);
				}else{
					redirect('app/logout','refresh');
				}	

		}
}