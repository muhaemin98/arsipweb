<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();		
		$this->load->model('Arsip_model');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "Admin") {
			
			$this->load->view('dashboard');

		}else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level') == "User") {

			 $query= $this->Arsip_model->diagram();

				    	$record = $query->result();
            			$data = [];
            			foreach($record as $row) {
                    	$data['label'][] = $row->bidang;
                    	$data['data'][] = (int) $row->count;
            			}
            			$x['chart_data'] = json_encode($data);

			$this->load->view('dashboard_user',$x);
		}else{
			redirect('app/logout');
		}
	}
}
