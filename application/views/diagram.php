<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>


<!-- Main content -->
<section class="content">

   <div class="box">
            <!-- /.box-header -->
            <div class="box-body" style="margin-left: 250px;">
              <div class="row">
                  <div class="col-md-8">
                   <canvas id="pie-chart"></canvas>
                 </div>
              </div>
            </div>
            <!-- /.box-body -->
   </div>

</section><!-- /.content -->

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>