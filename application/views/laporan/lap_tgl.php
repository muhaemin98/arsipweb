
<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <?php echo form_open_multipart('lap_tgl/set','class="form-horizontal"'); ?>

          <div class="col-xs-12">
            <div class="box box">
              <div class="box-header">
                <h3 style="color: green;margin-right: 10px;"><b>Laporan Pertanggal</b></h3>
              <div class="box-body">                
                <div class="col-md-5">
                  <div class="form-group">
                    <label for="tanggal_awal">Tanggal Awal</label>
                    <input class="form-control" type="date" name="tanggal_awal">                              
                  </div>
                </div><!-- /.form-group -->               
                <div class="col-md-5">
                  <div class="form-group">
                    <label for="tanggal_awal">Tanggal Akhir</label>
                    <input class="form-control" type="date" name="tanggal_akhir">                    
                  </div>
                </div><!-- /.form-group -->
                <div class="col-md-2">
                  <div class="form-group">
                    <label for="tanggal_awal">Cari</label>
                    <button type="submit" class="btn btn-primary form-control"><i class="glyphicon glyphicon-search"></i></button>                    
                  </div>
                </div><!-- /.form-group -->
              </div>
              
            </div>
          </div>
          <!-- /.col-md-12 -->
        </div>
        <?php echo form_close(); ?>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="50px">No</th>
                    <th>Nama Arsip</th>
                    <th>Bidang</th>
                    <th>Kategori</th>
                    <th>Tanggal Arsip</th>
                    <th>File Arsip</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                        $no=1;
                        foreach ($arsip->result_array() as $dp) 
                        {
                    ?>        
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $dp['nama_arsip']; ?></td>
                  <td><?php echo $dp['bidang']; ?></td>
                  <td><?php echo $dp['kategori']; ?></td>
                  <td><?php echo format_hari_tanggal($dp['tgl_arsip']); ?></td>
                  <td><a href="<?php echo base_url();?>assets/file_upload/<?php echo $dp['foto']; ?>"><button class="btn-small btn-success"><?php echo $dp['foto']; ?></button></a></td>
                </tr>
                <?php
                    $no++;
                      }
                ?>     
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
      <!-- /.row (main row) -->

    </section>

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>