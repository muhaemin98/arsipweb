<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Halaman Kategori
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url(); ?>kategori">Kategori</a></li>
        <li class="active">Edit Kategori</li>
    </ol>
</section>
<?php
    foreach($kategori->result_array()as $i):
      $id_kategori=$i['id_kategori'];
      $kategori=$i['kategori'];
?>
<!-- Main content -->
<section class="content">

   <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Ubah Data Kategori</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form role="form" action="<?php echo base_url(); ?>kategori/simpan_ubah" method="post">
        <!-- text input -->
        <div class="form-group">
           <input type="hidden" name="id_kategori" class="form-control" value="<?php echo $id_kategori ?>" required="">
        </div>
          <label>Kategori</label>
          <input type="text" name="kategori" class="form-control" value="<?php echo $kategori ?>" required="">
        </div>
        <br>
        <!-- textarea -->
        <div class="form-group" style="background-color: white;margin-left: 10px;">
          <button type="submit" class="btn btn-success">Simpan</button>
          <a href="<?php echo site_url('kategori')?>" class="btn btn-warning">Batal</a>
          <br>
          <br>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>

</section><!-- /.content -->
<?php endforeach;?>
<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>