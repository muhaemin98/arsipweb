<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Halaman Kategori
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">kategori</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

   <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Kategori</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <p align="left">
                <a href="<?php echo base_url(); ?>kategori/tambah_v" class="btn btn-primary"><i class="glyphicon glyphicon-plus glyphicon-white"></i> Tambah Data</a></p>

                <!------ Message berhasil atau tidak ---------->
                      <?php echo $this->session->userdata('message'); ?>

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>Nama Kategori</th>
                  <th width="50px">Aksi</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                        $no=1;
                        foreach ($kategori->result_array() as $dp) 
                        {
                    ?>        
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $dp['kategori']; ?></td>
                  <td>
                    <a href="<?php echo base_url(); ?>kategori/edit/<?php echo $dp['id_kategori']; ?>"title="ubah_v"><button class="btn btn-xs btn-success"><i class="ace-icon fa fa-pencil"></i></button>
                   
                   <a href="<?php echo base_url(); ?>kategori/hapus/<?php echo $dp['id_kategori']; ?>"onClick="return confirm('Anda Yakin..??');"title="hapus_v"><button class="btn btn-xs btn-danger"><i class="ace-icon fa fa-trash-o"></i></button>
                  </td>
                </tr>
                <?php
                    $no++;
                      }
                ?>     
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

</section><!-- /.content -->

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>