<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Halaman Kategori
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('kategori'); ?>">Kategori</a></li>
        <li class="active">Tambah data</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

   <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Tambah Data Kategori</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form role="form" action="<?php echo base_url(); ?>kategori/simpan" method="post">
        <!-- text input -->
        <div class="form-group">
          <label>Kategori</label>
          <input type="text" name="kategori" class="form-control" placeholder="kategori" required="">
        </div>
        <!-- textarea -->
        <div class="form-group">
          <button type="submit" class="btn btn-success">Simpan</button>
          <a href="<?php echo site_url('kategori')?>" class="btn btn-warning">Batal</a>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>

</section><!-- /.content -->

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>