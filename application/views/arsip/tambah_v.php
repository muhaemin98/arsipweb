<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Halaman Data Arsip
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php  echo site_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('arsip'); ?>">Arsip</a></li>
        <li class="active">Tambah Data</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

   <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tambah Data Arsip</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php echo form_open_multipart('arsip/simpan','role="form"'); ?>
               <!-- text input -->
              <div class="form-group">
               <label>Nama Arsip</label>
                <input type="text" name="nama_arsip" class="form-control" placeholder="nama arsip" required="">
               </div>

               <div class="form-group">
               <label>Bidang</label>
               <select class="form-control" name="id_bidang" required="">
                 <option value="0">-Pilih Bidang-</option>
                 <?php
                    foreach($bidang->result_array() as $b)
                    {
                      if($id_bidang==$b['id_bidang'])
                    {
                 ?>
                    <option value="<?php echo $b['id_bidang']; ?>" selected="selected"><?php echo $b['bidang']; ?></option>
                 <?php       
                    }
                    else
                    {
                 ?>   
                    <option value="<?php echo $b['id_bidang']; ?>"><?php echo $b['bidang']; ?></option>
                 <?php       
                      }
                    }
                  ?>  
               </select>
               </div>

                <div class="form-group">
               <label>Kategori</label>
               <select class="form-control" name="id_kategori" required="">
                 <option value="0">-Pilih Kategori-</option>
                 <?php
                    foreach($kategori->result_array() as $mg)
                    {
                      if($id_kategori==$mg['id_kategori'])
                    {
                 ?>
                    <option value="<?php echo $mg['id_kategori']; ?>" selected="selected"><?php echo $mg['kategori']; ?></option>
                 <?php       
                    }
                    else
                    {
                 ?>   
                    <option value="<?php echo $mg['id_kategori']; ?>"><?php echo $mg['kategori']; ?></option>
                 <?php       
                      }
                    }
                  ?>  
               </select>
               </div>

                <div class="form-group">
               <label>Tanggal Arsip</label>
                <input type="date" name="tgl_arsip" class="form-control">
               </div>
                <div class="form-group">
               <label>File Arsip</label>
                <input type="file" name="userfile" class="form-control">
               </div>
               <!-- textarea -->
                <div class="form-group">
                 <button type="submit" class="btn btn-success">Simpan</button>
                   <a href="<?php echo site_url('arsip')?>" class="btn btn-warning">Batal</a>
               </div>
            <!-- /.box-body -->
          </div>
                      <input type="hidden" name="id_param" value="<?php echo $id_param; ?>">
                      <input type="hidden" name="st" value="<?php echo $st; ?>">

        <?php echo form_close(); ?>

</section><!-- /.content -->

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>