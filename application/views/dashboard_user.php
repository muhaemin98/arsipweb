<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control Panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Selamat Datang</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <!-- Small boxes (Stat box) -->
      <div class="row">
              <!-- ./col -->
              <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
              <div class="inner">
              <h3><?php $jumlah=$this->db->get('arsip')->num_rows();echo $jumlah?></h3>


              <p>Data Arsip</p>
              </div>
              <div class="icon">
              <i class="fa fa-archive"></i>
              </div>
              <a href="<?php echo base_url(); ?>arsip" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-purple">
              <div class="inner">
              <h3><?php $jumlah=$this->db->get('arsip')->num_rows();echo $jumlah?></h3>


              <p>Laporan Arsip Pertanggal</p>
              </div>
              <div class="icon">
              <i class="fa fa-calendar"></i>
              </div>
              <a href="<?php echo base_url(); ?>lap_tgl" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
              <div class="inner">
              <h3><?php $jumlah=$this->db->get('arsip')->num_rows();echo $jumlah?></h3>

              
              <p>Laporan Arsip Perkategori</p>
              </div>
              <div class="icon">
              <i class="fa fa-navicon"></i>
              </div>
              <a href="<?php echo base_url(); ?>lap_ktg" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              </div>
        <!-- ./col -->
        <!-- ./col -->
              <div class="col-lg-12 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-gray">
              <div class="inner">
                
                <div class="row">
                <div class="col-md-4">
                   <canvas id="pie-chart"></canvas>
                 </div>
                 <div class="col-md-8">
                  <h3 style="color:black;"> Selamat Datang </h3>
                  <p style="color:black;text-align: justify;"> Selamat datang di Arsip Digital Badan Kesatuan Bangsa Dan Politik (Kesbangpol) Kota Pekanbaru. Website ini sebagai sarana untuk memberikan Informasi seputar pengarsipan data di Badan Kesatuan Bangsa Dan Politik (Kesbangpol) Kota Pekanbaru, dalam rangka mempercepat pencarian data dengan memanfaatkan arsip digital. Sesuai komitmen, Pemerintah Kota Pekanbaru siap menyelenggarakan sistem E-Government. Sebagaimana komitmen tersebut Badan Kesatuan Bangsa Dan Politik (Kesbangpol) Kota Pekanbaru berupaya menyelenggarakan tugas dan fungsinya dalam membantu Kepala Daerah Kota Pekanbaru dalam penyelenggaraan pemerintahan. </p>
                 </div>


              </div>
              </div>
              </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
        </div><!-- /.box-body -->
        <div class="box-footer" style="color: red;">
            Note : Pilih menu pintasan diatas untuk melihat data dan laporan arsip !
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

</section><!-- /.content -->

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>