<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">

                <?php 
                    if ($this->session->userdata('level') == 'Admin'){
                ?>
                <img src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/img/administrator.jpg') ?>" class="img-circle" alt="User Image" />

                <?php
                    }else{
                ?>

                <img src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/img/user.jpg') ?>" class="img-circle" alt="User Image" />

                <?php
                    }
                ?>

            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('nama_lengkap'); ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li>
                <a href="<?php echo base_url(); ?>dashboard">
                    <i class="fa fa-home"></i> <span>Home</span> 
                </a>
            </li>

            <?php
                if ($this->session->userdata('level')=="Admin") {
                    echo '
                    <li>
                        <a href="'.base_url().'kategori">
                        <i class="fa fa-sliders"></i> <span>Kategori</span> 
                        </a>
                    </li>
                    <li>
                        <a href="'.base_url().'bidang">
                        <i class="fa fa-tasks"></i> <span>Bidang</span> 
                        </a>
                    </li>';              
                }
            ?>

            <li>
                <a href="<?php echo base_url(); ?>arsip">
                    <i class="fa fa-archive"></i> <span>Arsip</span> 
                </a>
            </li>         
            <li class="treeview">
                <a>
                    <i class="fa fa-book"></i>
                    <span>Laporan</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('lap_ktg'); ?>"><i class="fa fa-circle-o"></i> PerKategori</a></li>
                    <li><a href="<?php echo site_url('lap_tgl'); ?>"><i class="fa fa-circle-o"></i> PerTanggal</a></li>
                    <li><a href="<?php echo site_url('diagram'); ?>"><i class="fa fa-circle-o"></i> Diagram rekapitulasi data</a></li>
                </ul>
            </li>

    <?php
        if ($this->session->userdata('level')=="Admin") {
            echo '            
            <li>
            <a href="'.base_url().'pengguna"><i class="fa fa-user"></i> Pengguna</a>
            </li>';
        } 
    ?>


            <li><a href="<?php echo site_url('app/logout'); ?>"><i class="fa fa-power-off"></i> Keluar</a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">