</head>
<body class="skin-green">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <a href="#" class="logo">
                <!--------<b>ARSIP</b>KESBANGPOL</a>----->
                <img class="logo" style="margin-left: -20px;" src="<?php echo base_url(); ?>assets/images/head.png" alt="Theme-Logo" /></a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php 
                                    if ($this->session->userdata('level') == 'Admin'){
                                ?>
                                <img src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/img/administrator.jpg') ?>" class="user-image" alt="User Image"/>

                                <?php
                                    }else{
                                ?>

                                <img src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/img/user.jpg') ?>" class="user-image" alt="User Image" />

                                <?php
                                    }
                                ?>


                                <span class="hidden-xs"><?php echo $this->session->userdata('nama_lengkap'); ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">

                                    <?php 
                                    if ($this->session->userdata('level') == 'Admin'){
                                    ?>

                                    <img src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/img/administrator.jpg') ?>" class="img-circle" alt="User Image" />

                                    <?php
                                        }else{
                                     ?>
                                    
                                    <img src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/img/user.jpg') ?>" class="img-circle" alt="User Image" />

                                    <?php
                                        }
                                    ?>

                                    <p>
                                        <?php echo $this->session->userdata('nama_lengkap'); ?>
                                    </p>
                                    <small style="color:yellow;"><?php echo $this->session->userdata('level'); ?></small>
                                </li>
     
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo site_url('app/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->