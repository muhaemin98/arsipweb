<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Halaman Data Arsip
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Arsip</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

   <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Arsip</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>Nama Arsip</th>
                  <th>Bidang</th>
                  <th>Kategori</th>
                  <th>Tanggal Arsip</th>
                  <th>File Arsip</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                        $no=1;
                        foreach ($arsip->result_array() as $dp) 
                        {
                    ?>        
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $dp['nama_arsip']; ?></td>
                  <td><?php echo $dp['bidang']; ?></td>
                  <td><?php echo $dp['kategori']; ?></td>
                  <td><?php echo format_hari_tanggal($dp['tgl_arsip']); ?></td>
                  <td><a href="<?php echo base_url();?>assets/file_upload/<?php echo $dp['foto']; ?>"><button class="btn-small btn-success"><?php echo $dp['foto']; ?></button></a></td>
                </tr>
                <?php
                    $no++;
                      }
                ?>     
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

</section><!-- /.content -->

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>