<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Halaman Bidang
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('bidang'); ?>">Bidang</a></li>
        <li class="active">Tambah data</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

   <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Tambah Data Bidang</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form role="form" action="<?php echo base_url(); ?>bidang/simpan" method="post">
        <!-- text input -->
        <div class="form-group">
          <label>Bidang</label>
          <input type="text" name="bidang" class="form-control" placeholder="bidang" required="">
        </div>
        <!-- textarea -->
        <div class="form-group">
          <button type="submit" class="btn btn-success">Simpan</button>
          <a href="<?php echo site_url('kategori')?>" class="btn btn-warning">Batal</a>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>

</section><!-- /.content -->

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>