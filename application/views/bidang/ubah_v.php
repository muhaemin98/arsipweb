<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Halaman Bidang
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url(); ?>bidang">Bidang</a></li>
        <li class="active">Edit Bidang</li>
    </ol>
</section>
<?php
    foreach($bidang->result_array()as $i):
      $id_bidang=$i['id_bidang'];
      $bidang=$i['bidang'];
?>
<!-- Main content -->
<section class="content">

   <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Ubah Data Bidang</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form role="form" action="<?php echo base_url(); ?>bidang/simpan_ubah" method="post">
        <!-- text input -->
        <div class="form-group">
           <input type="hidden" name="id_bidang" class="form-control" value="<?php echo $id_bidang ?>" required="">
        </div>
          <label>Kategori</label>
          <input type="text" name="bidang" class="form-control" value="<?php echo $bidang ?>" required="">
        </div>
        <br>
        <!-- textarea -->
        <div class="form-group" style="background-color: white;margin-left: 10px;">
          <button type="submit" class="btn btn-success">Simpan</button>
          <a href="<?php echo site_url('bidang')?>" class="btn btn-warning">Batal</a>
          <br>
          <br>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>

</section><!-- /.content -->
<?php endforeach;?>
<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>