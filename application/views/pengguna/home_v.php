<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Halaman Data Pengguna
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>pengguna"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Pengguna</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

   <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Pengguna</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <p align="left">
                <a href="<?php echo base_url(); ?>pengguna/tambah" class="btn btn-primary"><i class="glyphicon glyphicon-plus glyphicon-white"></i> Tambah Data</a></p>

                <!------ Message berhasil atau tidak ---------->
                      <?php echo $this->session->userdata('message'); ?>

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>Username</th>
                  <th>Nama Lengkap</th>
                  <th>Level</th>
                  <th width="50px">Aksi</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                        $no=1;
                        foreach ($pengguna->result_array() as $dp) 
                        {
                    ?>        
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $dp['username']; ?></td>
                  <td><?php echo $dp['nama_lengkap']; ?></td>
                  <td><?php echo $dp['level']; ?></td>
                  <td>
                    <a href="<?php echo base_url(); ?>pengguna/edit/<?php echo $dp['id_user']; ?>"title="ubah_v"><button class="btn btn-xs btn-success"><i class="ace-icon fa fa-pencil"></i></button>
                   
                   <a href="<?php echo base_url(); ?>pengguna/hapus/<?php echo $dp['id_user']; ?>"onClick="return confirm('Anda Yakin..??');"title="hapus_v"><button class="btn btn-xs btn-danger"><i class="ace-icon fa fa-trash-o"></i></button>
                  </td>
                </tr>
                <?php
                    $no++;
                      }
                ?>     
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

</section><!-- /.content -->

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>