<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Halaman Data Pengguna
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('pengguna'); ?>">Pengguna</a></li>
        <li class="active">Tambah data</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

   <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Tambah Data Pengguna</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form role="form" action="<?php echo base_url(); ?>pengguna/simpan" method="post">
        <!-- text input -->
        <div class="form-group">
          <label>Username</label>
          <input type="text" name="username" class="form-control" placeholder="username" required="">
        </div>
        <div class="form-group">
          <label>password</label>
          <input type="password" name="password" class="form-control" placeholder="password" required="">
        </div>
        <div class="form-group">
          <label>Nama Lengkap</label>
          <input type="text" name="nama_lengkap" class="form-control" placeholder="nama Lengkap" required="">
        </div>
        <div class="form-group">
          <label>Level</label>
          <select class="form-control" name="level" required="required">
                <option value=""> Pilih level </option>

                    <?php
                      $admin = "";
                       $user = "";
                          if ($level == "Admin") {
                                $admin = 'selected="selected"';
                                $user = "";
                           } else if ($level == "User") {
                                $admin = '';
                                $user = 'selected="selected"';
                           } else {
                                $admin = '';
                                $user = '';
                                $kosong1 = 'selected="selected"';
                           }
                           ?>
                <option value="Admin" <?php echo $admin; ?>>Admin</option>
                 <option value="User" <?php echo $user; ?>>User</option>
          </select>
        </div>
        <!-- textarea -->
        <div class="form-group">
          <button type="submit" class="btn btn-success">Simpan</button>
          <a href="<?php echo site_url('pengguna')?>" class="btn btn-warning">Batal</a>
        </div>

             <input type="hidden" name="id_param" value="<?php echo $id_param; ?>">
             <input type="hidden" name="st" value="<?php echo $st; ?>">
      </form>
    </div>
    <!-- /.box-body -->
  </div>

</section><!-- /.content -->

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>