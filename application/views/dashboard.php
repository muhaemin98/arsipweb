<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control Panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Selamat Datang</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php $jumlah=$this->db->get('Kategori')->num_rows();echo $jumlah?></h3>

              <p>Kategori Arsip</p>
              </div>
              <div class="icon">
              <i class="fa fa-sliders"></i>
              </div>
              <a href="<?php echo base_url(); ?>kategori" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>    
              </div>


              <!-- ./col -->
              <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-primary">
              <div class="inner">
              <h3><?php $jumlah=$this->db->get('bidang')->num_rows();echo $jumlah?></h3>

              <p>Bidang Arsip</p>
              </div>
              <div class="icon">
              <i class="fa fa-tasks"></i>
              </div>
              <a href="<?php echo base_url(); ?>bidang" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              </div>
              <!-- ./col -->

              <!-- ./col -->
              <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
              <div class="inner">
              <h3><?php $jumlah=$this->db->get('user')->num_rows();echo $jumlah?></h3>

              <p>Data Pengguna</p>
              </div>
              <div class="icon">
              <i class="fa fa-user"></i>
              </div>
              <a href="<?php echo base_url(); ?>pengguna" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              </div>
              <!-- ./col -->

              <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
              <div class="inner">
              <h3><?php $jumlah=$this->db->get('arsip')->num_rows();echo $jumlah?></h3>


              <p>Data Arsip</p>
              </div>
              <div class="icon">
              <i class="fa fa-archive"></i>
              </div>
              <a href="<?php echo base_url(); ?>arsip" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-purple">
              <div class="inner">
              <h3><?php $jumlah=$this->db->get('arsip')->num_rows();echo $jumlah?></h3>


              <p>Laporan Arsip Pertanggal</p>
              </div>
              <div class="icon">
              <i class="fa fa-calendar"></i>
              </div>
              <a href="<?php echo base_url(); ?>lap_tgl" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
              <div class="inner">
              <h3><?php $jumlah=$this->db->get('arsip')->num_rows();echo $jumlah?></h3>

              
              <p>Laporan Arsip Perkategori</p>
              </div>
              <div class="icon">
              <i class="fa fa-navicon"></i>
              </div>
              <a href="<?php echo base_url(); ?>lap_ktg" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
        </div><!-- /.box-body -->
        <div class="box-footer" style="color: red;">
            Note : Anda dapat memilih menu yang terdapat di control panel untuk mengelola data.
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

</section><!-- /.content -->

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>